package my.maven.project.group;

/**
 * Created by student3 on 20.06.2017.
 */
public class person {
    private String Name;
    private String SurName;
    private String ParentName;

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getSurName() {
        return SurName;
    }

    public void setSurName(String surName) {
        SurName = surName;
    }

    public String getParentName() {
        return ParentName;
    }

    public void setParentName(String parentName) {
        ParentName = parentName;
    }
}
