package my.maven.project.group;
import com.google.gson.Gson;

import java.io.FileWriter;
import java.io.IOException;

public class GsonToJson {
    public static void main( String[] args )
    {
        person personObj = new person();
        personObj.setName(args[0]);
        personObj.setSurName(args[1]);
        personObj.setParentName(args[2]);

        Gson gson = new Gson();

        String json = gson.toJson(personObj);

        try
        {
            FileWriter writer = new FileWriter("out.txt", false);
            writer.write(json);
            writer.flush();
        }
        catch(IOException ex)
        {
            ex.printStackTrace();
        }
    }
}
